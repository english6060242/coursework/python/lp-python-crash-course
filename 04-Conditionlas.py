# Conditionals in Python:

# Conditional Statements:
# Conditional statements allow you to execute different blocks of code based on the evaluation of conditions.
# The basic conditional statements in Python are 'if', 'elif' (short for 'else if'), and 'else'.

# Syntax:
# if condition:
#     code_block_1
# elif condition:
#     code_block_2
# else:
#     code_block_3

# Example:
# Taking user input to demonstrate conditional statements:
num = int(input("Enter a number: "))

# if statement:
# Executes the code block if the condition is True.
# If the condition is False, the code block is skipped.
if num > 0:
    print("Number is positive")

# elif statement:
# Allows for checking additional conditions if the preceding 'if' condition is False.
# It stands for 'else if'.
# Executes the code block if the condition is True.
# If the condition is False, the code block is skipped.
elif num < 0:
    print("Number is negative")

# else statement:
# Executes the code block if none of the preceding conditions are True.
# It is optional and can be omitted if not needed.
else:
    print("Number is zero")

# Multiple Conditions:
# You can use logical operators ('and', 'or', 'not') to combine multiple conditions.
# Example:
age = int(input("Enter your age: "))

if age >= 18 and age <= 65:
    print("You are of working age.")
else:
    print("You are not of working age.")

# Nested Conditionals:
# You can nest one conditional statement inside another to create more complex logic.
# Example:
x = int(input("Enter a number: "))
if x > 0:
    if x % 2 == 0:
        print("Number is positive and even")
    else:
        print("Number is positive and odd")
elif x < 0:
    print("Number is negative")
else:
    print("Number is zero")

# Ternary Operator (Conditional Expression):
# Python also supports a ternary operator that allows for conditional expressions in a single line.
# Syntax: value_if_true if condition else value_if_false
# Example:
num = int(input("Enter a number: "))
result = "positive" if num > 0 else "negative or zero"
print("Number is", result)