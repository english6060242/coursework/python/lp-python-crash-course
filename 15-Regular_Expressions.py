# Regular Expressions (regex) in Python:

# Regular expressions are powerful tools for pattern matching and manipulating strings based on specified patterns.

# 1. Pattern Matching:
#   - A regular expression (regex or regexp) is a sequence of characters that define a search pattern.
#   - It allows you to specify rules for matching strings, such as character sequences, ranges, repetitions, etc.
#   - Special characters in regex include:
#       - '.' (dot): Matches any single character except newline.
#       - '^' (caret): Matches the start of the string.
#       - '$' (dollar): Matches the end of the string.
#       - '|' (pipe): Alternation, matches either the expression before or after the pipe.
#       - '?' (question mark): Matches zero or one occurrence of the preceding element.
#       - '*' (asterisk): Matches zero or more occurrences of the preceding element.
#       - '+' (plus): Matches one or more occurrences of the preceding element.
#       - '\d': Matches any digit (equivalent to [0-9]).
#       - '\w': Matches any alphanumeric character (equivalent to [a-zA-Z0-9_]).
#       - '\s': Matches any whitespace character.
import re

pattern = r'\bcat\b'  # Matches the word 'cat' as a whole word
text = "The cat is sitting on the mat. A catatonic state is not good for a cat."

# 2. Searching and Matching Strings:
#   - Python's 're' module provides functions like 'search()', 'match()', and 'findall()' for searching and matching strings using regex.
#   - 'search()': Searches the entire string for a match and returns the first occurrence.
#   - 'match()': Matches the regex pattern only at the beginning of the string.
#   - 'findall()': Finds all occurrences of the pattern in the string and returns them as a list.
result = re.search(pattern, text)
print("Search Result:", result.group() if result else "No match found")  # Output: cat

# 3. Substitution:
#   - The 'sub()' function in the 're' module is used to substitute occurrences of a pattern in a string with another string.
#   - Syntax: re.sub(pattern, replacement, string)
modified_text = re.sub(pattern, "dog", text)
print("Modified Text:", modified_text)  # Output: The dog is sitting on the mat. A catatonic state is not good for a cat.

# 4. Splitting:
#   - The 'split()' function in the 're' module is used to split a string into substrings based on a regex pattern.
#   - Syntax: re.split(pattern, string)
split_text = re.split(r'\W+', text)  # Splitting on non-word characters
print("Split Text:", split_text)  # Output: ['The', 'cat', 'is', 'sitting', 'on', 'the', 'mat', 'A', 'catatonic', 'state', 'is', 'not', 'good', 'for', 'a', 'cat']