# Lists in Python:

# Lists are one of the most versatile and commonly used data structures in Python.
# They allow you to store and manipulate collections of items in an ordered sequence.

# Creating Lists:
# Lists are created by enclosing comma-separated values within square brackets '[ ]'.
# Lists can contain elements of different data types, including numbers, strings, or even other lists.
fruits = ["apple", "banana", "cherry"]
print("Fruits:", fruits)

# Accessing Elements:
# You can access elements of a list using square brackets '[ ]' and the index of the element.
# Indexing starts from 0 for the first element and goes up to (length - 1).
# Negative indices count from the end of the list, with -1 referring to the last element.
print("First fruit:", fruits[0])  # Output: "apple"

# Adding Elements:
# You can add elements to a list using various methods, such as 'append()', 'insert()', or list concatenation.
# - 'append()' adds a single element to the end of the list.
# - 'insert()' adds an element at a specific position in the list.
# - List concatenation '+' combines two lists into a single list.
fruits.append("orange")
print("Fruits after adding:", fruits)  # Output: ['apple', 'banana', 'cherry', 'orange']

# Modifying Elements:
# You can modify elements of a list by directly assigning a new value to the desired index.
fruits[1] = "grape"
print("Fruits after modification:", fruits)  # Output: ['apple', 'grape', 'cherry', 'orange']

# Removing Elements:
# You can remove elements from a list using methods like 'pop()', 'remove()', or 'del'.
# - 'pop()' removes and returns the element at the specified index (default is the last element).
# - 'remove()' removes the first occurrence of a specified value from the list.
# - 'del' removes the element at a specified index or deletes the entire list.
fruits.pop(1)
print("Fruits after removing:", fruits)  # Output: ['apple', 'cherry', 'orange']

# List Slicing:
# You can extract a sublist (slice) from a list using the slice operator ':'.
# Syntax: list[start:end:step]
# - 'start' is the starting index (inclusive).
# - 'end' is the ending index (exclusive).
# - 'step' is the step size (optional, default is 1).
# If any of these parameters are omitted, they default to the beginning, end, and step size of the list, respectively.
numbers = [0, 1, 2, 3, 4, 5]
print("Sublist:", numbers[2:5])  # Output: [2, 3, 4]

# List Methods:
# Python provides various built-in methods for working with lists, including 'sort()', 'reverse()', 'count()', 'index()', etc.
# These methods allow you to manipulate, sort, search, and perform other operations on lists efficiently.