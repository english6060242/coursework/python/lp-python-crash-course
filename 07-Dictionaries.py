# Dictionaries in Python:

# Dictionaries are unordered collections of items that are stored as key-value pairs.
# They are versatile data structures used for mapping keys to values, similar to a real-world dictionary.

# Creating Dictionaries:
# Dictionaries are created by enclosing comma-separated key-value pairs within curly braces '{ }'.
# Each key-value pair is separated by a colon ':'.
# Keys must be immutable (e.g., strings, numbers, tuples), while values can be of any data type.
student = {
    "name": "Alice",
    "age": 20,
    "major": "Computer Science"
}
print("Student:", student)

# Accessing Elements:
# You can access elements of a dictionary using square brackets '[ ]' and the key of the element.
# Simply provide the key to retrieve the corresponding value from the dictionary.
print("Student's age:", student["age"])  # Output: 20

# Adding Elements:
# You can add new key-value pairs to a dictionary using square brackets '[ ]' and assignment '='.
# Simply provide a new key and assign a value to it to add it to the dictionary.
student["grade"] = "A"
print("Student after adding grade:", student)

# Modifying Elements:
# You can modify the value of an existing key in a dictionary by directly assigning a new value to it.
# Simply provide the key and assign the new value to update the existing key-value pair.
student["age"] = 21
print("Student after modifying age:", student)

# Removing Elements:
# You can remove key-value pairs from a dictionary using the 'del' keyword or the 'pop()' method.
# - 'del' removes the specified key-value pair from the dictionary.
# - 'pop()' removes and returns the value associated with the specified key.
# Note: If the key does not exist, 'del' will raise a KeyError, while 'pop()' will return a specified default value or raise a KeyError.
del student["major"]
print("Student after removing major:", student)

# Dictionary Methods:
# Python provides various built-in methods for working with dictionaries, including 'keys()', 'values()', 'items()', 'update()', 'clear()', etc.
# These methods allow you to manipulate, iterate over, and perform other operations on dictionaries efficiently.