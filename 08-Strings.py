# Strings in Python:

# Strings are immutable sequences of characters, which are enclosed in single (' '), double (" "), or triple (''' ''' or """ """) quotes.
# They are used to represent text data and can contain letters, numbers, symbols, and whitespace.

# Creating Strings:
# Strings can be created using single quotes (' '), double quotes (" "), or triple quotes (''' ''' or """ """).
# Triple quotes are used for multiline strings or strings containing single or double quotes.
message = "Hello, World!"
print("Message:", message)

# Concatenation:
# Strings can be concatenated using the '+' operator, which joins two or more strings together.
greeting = "Hello"
name = "Alice"
welcome_message = greeting + ", " + name + "!"
print("Welcome Message:", welcome_message)

# String Formatting:
# Python provides multiple ways to format strings, including using the 'format()' method and f-strings (formatted string literals).
# - 'format()' method: Allows for inserting variable values into a string using placeholders {}.
# - f-strings: Introduced in Python 3.6, f-strings allow for embedding expressions and variables directly into strings using {}.
age = 30
formatted_message = "My age is {}.".format(age)
print("Formatted Message:", formatted_message)

# Uppercase and Lowercase:
# Strings have built-in methods like 'upper()' and 'lower()' to convert characters to uppercase and lowercase, respectively.
print("Uppercase:", message.upper())  # Output: "HELLO, WORLD!"
print("Lowercase:", message.lower())  # Output: "hello, world!"

# String Length:
# The 'len()' function returns the length of a string, i.e., the number of characters in the string.
print("Length:", len(message))  # Output: 13

# Accessing Characters:
# Individual characters in a string can be accessed using indexing and slicing.
# Indexing starts from 0 for the first character and goes up to (length - 1).
# Negative indices count from the end of the string, with -1 referring to the last character.
print("First Character:", message[0])   # Output: "H"
print("Last Character:", message[-1])   # Output: "!"

# String Methods:
# Python provides numerous built-in methods for working with strings, including 'split()', 'join()', 'replace()', 'strip()', 'startswith()', 'endswith()', etc.
# These methods allow you to manipulate, search, and perform various operations on strings efficiently.