# Sets in Python:

# Sets are unordered collections of unique elements. They are used to store distinct items without duplicates.

# 1. Creating Sets:
#   - Sets are created by enclosing comma-separated values within curly braces '{ }'.
#   - Empty set: my_set = set()
#   - Set with elements: my_set = {value1, value2, ...}
#   - Note: To create an empty set using curly braces, use set() as {} creates an empty dictionary.
my_set = {1, 2, 3, 4, 5}
print("Set:", my_set)  # Output: {1, 2, 3, 4, 5}

# 2. Adding Elements:
#   - Elements can be added to a set using the 'add()' method.
#   - Adding an existing element has no effect as sets only contain unique elements.
my_set.add(6)
print("After adding:", my_set)  # Output: {1, 2, 3, 4, 5, 6}

# 3. Removing Elements:
#   - Elements can be removed from a set using the 'remove()' or 'discard()' method.
#   - 'remove()' raises KeyError if the element does not exist, while 'discard()' does not.
my_set.remove(3)
print("After removing:", my_set)  # Output: {1, 2, 4, 5, 6}

# 4. Set Operations:
#   - Union: Combines elements from two sets, excluding duplicates.
#   - Intersection: Returns elements common to both sets.
#   - Difference: Returns elements present in the first set but not in the second.
#   - Symmetric Difference: Returns elements present in either set, but not in both.
set1 = {1, 2, 3}
set2 = {3, 4, 5}
print("Union:", set1.union(set2))  # Output: {1, 2, 3, 4, 5}
print("Intersection:", set1.intersection(set2))  # Output: {3}
print("Difference:", set1.difference(set2))  # Output: {1, 2}
print("Symmetric Difference:", set1.symmetric_difference(set2))  # Output: {1, 2, 4, 5}

# 5. Set Methods:
#   - 'clear()': Removes all elements from the set.
#   - 'copy()': Returns a shallow copy of the set.
#   - 'pop()': Removes and returns an arbitrary element from the set.
#   - 'update()': Updates the set with elements from another set or iterable.
#   - 'discard()': Removes a specified element from the set (if present).
#   - 'remove()': Removes a specified element from the set.
#   - 'isdisjoint()': Checks if two sets have no common elements.
#   - 'issubset()': Checks if all elements of one set are present in another set.
#   - 'issuperset()': Checks if all elements of another set are present in the set.
#   - 'len()': Returns the number of elements in the set.
#   - 'max()': Returns the maximum value in the set.
#   - 'min()': Returns the minimum value in the set.
#   - 'sum()': Returns the sum of all elements in the set (if numeric).
#   - Note: Some methods like 'update()', 'discard()', 'remove()', etc., modify the set in place.

# 6. Frozen Sets:
#   - Frozen sets are immutable versions of sets and can be created using the 'frozenset()' function.
#   - They are hashable and can be used as keys in dictionaries or elements in other sets.
frozen_set = frozenset([1, 2, 3])
print("Frozen Set:", frozen_set)  # Output: frozenset({1, 2, 3})

# 7. Use Cases:
#   - Sets are useful for removing duplicates from a collection, testing membership, and performing set operations efficiently.
#   - They are commonly used in mathematical computations, data analysis, and algorithm implementations.