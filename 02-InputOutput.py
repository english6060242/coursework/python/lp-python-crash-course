# Input and output
greetings = 0
name = input("Enter your name: ")
greetings += 1
print("Hello,", name + "! I'm a python program and I've given", greetings, "greetings")

name = input("Enter your name: ")
greetings += 1
print("Hello,", name + "! I'm a python program and I've given", greetings, "greetings")

# When printing, you can concatenate multiple variables and multiple values.
# String values ("Hello") and string variables (name = "peter") can be concatenated using '+'.
# We can concatenate string values or variables with non-string values using a comma ','.

# 'input' is a Python function that returns a value (whatever the user has inputted). This means this value
# can be used inside other functions:
print(input("Single line input/print - Please enter a value: "))

