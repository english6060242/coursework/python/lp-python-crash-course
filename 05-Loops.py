# Loops in Python:

# Loops allow you to iterate over a sequence of elements and execute a block of code repeatedly.

# 1. for Loop:
# The 'for' loop is used to iterate over a sequence (e.g., a list, tuple, range, or string).
# It executes the code block for each item in the sequence.

# Syntax:
# for item in sequence:
#     code_block

# Example:
# Looping through a range of numbers:
for i in range(5): # from 0 to 4
    print("Iteration:", i)

# Explanation:
# - 'range(5)' generates a sequence of numbers from 0 to 4 (excluding 5).
# - The loop iterates over each number in the range.
# - In each iteration, the value of 'i' is assigned to the current number in the sequence.
# - The code block inside the loop is executed for each iteration, printing the value of 'i'.

# Common Patterns with for Loops:
# - Looping through a range of numbers: 'for i in range(start, stop, step)'
# - Looping through elements of a list: 'for item in list'
# - Looping through characters of a string: 'for char in string'

# 2. while Loop:
# The 'while' loop is used to execute a block of code as long as a specified condition is True.
# It continues to execute the code block repeatedly until the condition becomes False.

# Syntax:
# while condition:
#     code_block

# Example:
# Countdown using a while loop:
count = 5
while count > 0:
    print(count)
    count -= 1

# Explanation:
# - The loop continues as long as the condition 'count > 0' is True.
# - In each iteration, the value of 'count' is printed, and then decremented by 1.
# - The loop terminates when 'count' becomes 0 and the condition becomes False.

# Infinite Loops:
# Be cautious when using while loops to avoid creating infinite loops.
# An infinite loop occurs when the loop condition is always True, causing the loop to run indefinitely.
# To avoid infinite loops, ensure that the loop condition eventually becomes False.

# Loop Control Statements:
# Python provides loop control statements like 'break', 'continue', and 'pass' to control the flow of loops.
# - 'break' terminates the loop prematurely.
# - 'continue' skips the rest of the code block and proceeds to the next iteration.
# - 'pass' is a null operation that does nothing and can be used as a placeholder.
