# Modules in Python:

# Modules are files containing Python code that define functions, classes, and variables.
# They allow you to organize and reuse code by grouping related functionalities together.
# Python provides a rich collection of built-in modules, and you can also create and use your own modules.

# Using Modules:
# To use a module in your Python script, you need to import it using the 'import' keyword.
# Once imported, you can access the functions, classes, and variables defined in the module using dot notation.

# Example 1: Using the math Module
import math

# Module Functions:
# You can access functions defined in a module using dot notation (module_name.function_name()).
# Example: math.sqrt() calculates the square root of a number.
print("Square root of 16:", math.sqrt(16))  # Output: 4.0

# Module Variables:
# Modules may also define variables that can be accessed using dot notation.
# Example: math.pi gives the value of pi (π).
print("Value of pi:", math.pi)  # Output: 3.141592653589793

# Example 2: Using the random Module
import random

# Module Functions:
# The random module provides functions for generating random numbers and performing random selections.
# Example: random.randint() generates a random integer within a specified range.
random_number = random.randint(1, 100)
print("Random number between 1 and 100:", random_number)

# Example 3: Using the datetime Module
import datetime

# Module Classes and Functions:
# The datetime module provides classes and functions for working with dates and times.
# Example: datetime.datetime.now() returns the current date and time.
current_datetime = datetime.datetime.now()
print("Current date and time:", current_datetime)

# Example 4: Using the os Module
import os

# Module Functions:
# The os module provides functions for interacting with the operating system.
# Example: os.getcwd() returns the current working directory.
current_directory = os.getcwd()
print("Current working directory:", current_directory)

# Example 5: Using the sys Module
import sys

# Module Variables and Functions:
# The sys module provides variables and functions related to the Python interpreter and system environment.
# Example: sys.version gives the Python interpreter's version.
print("Python version:", sys.version)

#Documentation for each module can be found in the Python Standard Library documentation: https://docs.python.org/3/library/index.html 
# or by using the help() function in Python.

# help() #use 'q' to exit

# Using Separate Files for Functions and Importing into the Main File:

# 1. Modular Code Organization:
#    - Breaking code into smaller, manageable modules enhances readability, reusability, and maintainability.
#    - Placing related functions or classes in separate files promotes a modular design and allows for better organization of code.

# 2. Creating a Separate File for Functions:
#    - When certain functions are logically related but independent of the main code, it's beneficial to define them in a separate file.
#    - This separate file acts as a module containing the functions, making it easier to reuse them across multiple parts of the project.

## Example: File: my_functions.py
#def func1():
#    print("Function 1")

#def func2():
#    print("Function 2")

# 3. Importing Functions into the Main File:
#    - To use the functions defined in the separate file, import them into the main file using the 'import' statement.
#    - You can import individual functions or import the entire module.

## Example: File: main.py
#from my_functions import func1, func2

#def main():
#    func1()  # Call func1() from my_functions.py
#    func2()  # Call func2() from my_functions.py

#if __name__ == "__main__":
#    main()

# 4. Downsides of using 'import *':
#    - While using 'import *' allows you to import all functions from a module at once, it has several downsides:
#      - Namespace Pollution: 'import *' imports all names from the module into the current namespace, potentially causing name clashes with other identifiers.
#      - Lack of Clarity: It's not immediately clear which functions are being used from the module, making the code less readable.
#      - Potential Performance Overhead: Importing unnecessary functions may lead to a slight performance overhead, especially in larger projects.
#    - It's generally recommended to avoid 'import *' and import only the specific functions or objects needed from the module to maintain code clarity and avoid potential issues.