# Functions in Python:

# Function Definition:
# Functions in Python are defined using the 'def' keyword followed by the function name and parameters in parentheses.
# The function body is indented and contains the code to be executed when the function is called.
def greet(name):
    """This function greets the user with the given name."""
    return "Hello, " + name + "!"

# Function Call:
# To call a function, you simply use its name followed by parentheses containing the arguments (if any).
# The function call returns the value specified by the 'return' statement in the function definition.
# If there's no 'return' statement, the function returns 'None' by default.
print(greet("Bob"))  # Output: Hello, Bob!

# Function Parameters:
# Functions can accept zero or more parameters (also known as arguments).
# Parameters are variables that are used to pass data to the function.
# They are specified in the function definition inside the parentheses.
# Parameters can have default values, which are used if no value is provided when calling the function.
# Example:
def add_numbers(x, y=0):
    """This function adds two numbers."""
    return x + y

# Calling the function with different numbers of arguments:
print(add_numbers(3, 5))  # Output: 8
print(add_numbers(3))     # Output: 3 (default value of 'y' is used)

# Docstrings:
# Docstrings are optional documentation strings that describe what the function does.
# They are enclosed in triple quotes (""" """) and are written as the first statement in the function body.
# Docstrings can be accessed using the '__doc__' attribute of the function.
print(greet.__doc__)  # Output: This function greets the user with the given name.

# Return Values:
# Functions can return a value using the 'return' statement.
# The return value can be of any data type or object.
# Once a 'return' statement is executed, the function exits immediately, and no further code is executed.
# If there's no 'return' statement or 'return' without an expression, the function returns 'None' by default.
# Example:
def is_even(number):
    """This function checks if a number is even."""
    if number % 2 == 0:
        return True
    else:
        return False

# Calling the function and using the return value:
result = is_even(4)
print(result)  # Output: True

# Scope:
# Variables defined inside a function are local to that function and cannot be accessed outside it.
# Similarly, variables defined outside any function are global and can be accessed from anywhere in the code.
# Example:
x = 10  # Global variable

def multiply_by_global(y):
    """This function multiplies a number by a global variable."""
    return x * y

print(multiply_by_global(5))  # Output: 50 (10 * 5)

# Anonymous Functions (Lambda Functions):
# Lambda functions are small, anonymous functions defined using the 'lambda' keyword.
# They can have any number of parameters, but can only have one expression.
# Lambda functions are often used as arguments to higher-order functions like 'map', 'filter', and 'sorted'.
# Example:
double = lambda x: x * 2
print(double(5))  # Output: 10

#Example2
my_adder = lambda a,b : a + b 
print(my_adder(10,5)) # Output: 15