# Tuples in Python:

# Tuples are immutable sequences of elements, similar to lists, but with the key difference that tuples cannot be modified after creation.

# 1. Creating Tuples:
#   - Tuples are created by enclosing comma-separated values within parentheses '( )'.
#   - Empty tuple: my_tuple = ()
#   - Single-element tuple: my_tuple = (value,)
#   - Multiple elements tuple:
my_tuple = (1, 2, 3, "a", "b", "c")
print("Tuple:", my_tuple)  # Output: (1, 2, 3, 'a', 'b', 'c')

# 2. Accessing Elements:
#   - Elements of a tuple can be accessed using indexing (starting from 0) or negative indexing (counting from the end).
#   - Syntax: tuple_name[index]
print("First element:", my_tuple[0])    # Output: 1
print("Last element:", my_tuple[-1])    # Output: 'c'

# 3. Tuple Slicing:
#   - Similar to lists, tuple slicing allows you to extract a subset of elements from a tuple using the slice operator ':'.
#   - Syntax: tuple_name[start_index:end_index:step]
#   - If start_index or end_index is omitted, it defaults to the beginning or end of the tuple, respectively.
print("Slice:", my_tuple[1:4])  # Output: (2, 3, 'a')

# 4. Tuple Concatenation:
#   - Tuples can be concatenated using the '+' operator, which combines two tuples into a single tuple.
tuple1 = (1, 2, 3)
tuple2 = ('a', 'b', 'c')
concatenated_tuple = tuple1 + tuple2
print("Concatenated tuple:", concatenated_tuple)  # Output: (1, 2, 3, 'a', 'b', 'c')

# 5. Tuple Repetition:
#   - Tuples can be repeated using the '*' operator, which duplicates the elements of the tuple.
original_tuple = (1, 2)
repeated_tuple = original_tuple * 3
print("Repeated tuple:", repeated_tuple)  # Output: (1, 2, 1, 2, 1, 2)

# 6. Tuple Methods:
#   - Tuples have limited built-in methods due to their immutable nature.
#   - Methods include 'count()' to count occurrences of a value and 'index()' to find the index of a value.
print("Number of 'a's:", my_tuple.count('a'))  # Output: 1
print("Index of 'b':", my_tuple.index('b'))   # Output: 4

# 7. Tuple Unpacking:
#   - Tuple unpacking allows you to assign values of a tuple to multiple variables in a single statement.
#   - The number of variables must match the number of elements in the tuple.
x, y, z = (10, 20, 30)
print("Unpacked values:", x, y, z)  # Output: 10 20 30

# 8. Immutable Nature:
#   - Unlike lists, tuples cannot be modified after creation. Attempts to modify tuples result in TypeError.
#   - This immutability ensures data integrity and can be useful for protecting critical data.
#   - However, you can create a new tuple by concatenating or slicing existing tuples.