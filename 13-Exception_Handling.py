# Exception Handling in Python:

# Exception handling is a mechanism to handle runtime errors or exceptional situations that may occur during program execution.

# 1. Try-Except Block:
#   - The 'try' block is used to wrap the code that may potentially raise an exception.
#   - The 'except' block is used to catch and handle specific exceptions raised within the 'try' block.
#   - Syntax:
#       try:
#           # Code that may raise an exception
#       except ExceptionName1:
#           # Handle the specific exception ExceptionName1
#       except ExceptionName2:
#           # Handle the specific exception ExceptionName2
#       ...
#       except:
#           # Handle any other exceptions not caught by previous blocks
try:
    result = 10 / 0  # This will raise a ZeroDivisionError
except ZeroDivisionError:
    print("Error: Division by zero occurred!")

# 2. Handling Multiple Exceptions:
#   - You can handle multiple exceptions by specifying multiple 'except' blocks or by using tuple unpacking.
#   - In tuple unpacking, the 'except' block catches multiple exceptions separated by commas.
try:
    value = int("abc")  # This will raise a ValueError
except (ValueError, TypeError):  # Handling both ValueError and TypeError
    print("Error: Invalid value provided!")

# 3. Finally Block:
#   - The 'finally' block is optional and is used to execute cleanup code, irrespective of whether an exception occurs or not.
#   - It runs after the 'try' block and any matching 'except' blocks, even if no exception is raised.
#   - Syntax:
#       try:
#           # Code that may raise an exception
#       except ExceptionName:
#           # Handle the exception
#       finally:
#           # Code to execute whether an exception occurs or not
try:
    file = open("example.txt", "r")
    print(file.read())
except FileNotFoundError:
    print("Error: File not found!")
finally:
    if 'file' in locals() or 'file' in globals():
        file.close()

# 4. Raising Exceptions:
#   - You can explicitly raise exceptions using the 'raise' statement.
#   - This allows you to create custom exceptions or handle specific situations programmatically.
#   - Syntax: raise ExceptionClassName("Optional error message")
def divide(x, y):
    if y == 0:
        raise ValueError("Division by zero is not allowed!")
    return x / y

try:
    result = divide(10, 0)
except ValueError as ve:
    print("Error:", ve)

# 5. Exception Hierarchy:
#   - Python's built-in exceptions form a hierarchy where more specific exceptions inherit from broader ones.
#   - This allows you to catch specific exceptions while still handling their parent exceptions.
try:
    file = open("example.txt", "r")
    print(file.read())
except FileNotFoundError:
    print("Error: File not found!")
except Exception as e:
    print("Error:", e)

# 6. Custom Exceptions:
#   - You can create custom exception classes by inheriting from Python's built-in 'Exception' class.
#   - This allows you to define specific exception types tailored to your application's needs.
class CustomError(Exception):
    def __init__(self, message="Custom error occurred!"):
        self.message = message

try:
    raise CustomError("This is a custom error message.")
except CustomError as ce:
    print("Error:", ce.message)

# Common Situations Requiring Exception Handling:

# 1. File I/O Operations:
#    - Opening a file that does not exist.
try:
    with open("nonexistent_file.txt", "r") as file:
        content = file.read()
except FileNotFoundError:
    print("File not found")

# 2. User Input:
#    - Attempting to convert user input to a specific data type where the input is not valid.
try:
    num = int(input("Enter a number: "))
except ValueError:
    print("Invalid input. Please enter a valid integer.")

# 3. Network Operations:
#    - Making network requests or API calls where the network is unreachable or the server is down.
import requests
try:
    response = requests.get("http://example.com")
except requests.exceptions.RequestException:
    print("Failed to make network request")

# 4. Database Operations:
#    - Executing SQL queries or database transactions where the database is not available or the query is invalid.
import sqlite3
try:
    conn = sqlite3.connect("nonexistent_database.db")
except sqlite3.Error:
    print("Failed to connect to database")

# 5. Mathematical Operations:
#    - Performing arithmetic operations that may result in division by zero.
try:
    result = 10 / 0
except ZeroDivisionError:
    print("Division by zero")

# 6. Indexing and Slicing:
#    - Accessing elements in a list using invalid indices.
my_list = [1, 2, 3]
try:
    print(my_list[5])
except IndexError:
    print("Index out of range")

# 7. Iterating Over Collections:
#    - Iterating over a list and accessing elements that do not exist.
try:
    for i in range(5):
        print(my_list[i])
except IndexError:
    print("Index out of range")

# 8. External Libraries and APIs:
#    - Calling functions or methods from external libraries or APIs that may raise exceptions.
#      Example: Attempting to convert a string to an integer using invalid input.
try:
    num = int("abc")
except ValueError:
    print("Invalid conversion")

# 9. Concurrency and Multithreading:
#    - Handling race conditions and synchronization issues in concurrent or multithreaded programs.
import threading
shared_variable = 0
def increment():
    global shared_variable
    for _ in range(100000):
        shared_variable += 1

# Create multiple threads to increment the shared variable concurrently
threads = []
for _ in range(5):
    thread = threading.Thread(target=increment)
    thread.start()
for thread in threads:
    thread.join()

# 10. Custom Exceptions:
#     - Defining and using custom exception classes to encapsulate and communicate errors.
class CustomError(Exception):
    pass

# Example of raising a custom exception
try:
    raise CustomError("Custom exception message")
except CustomError as e:
    print("Custom exception:", e)
