# Object-Oriented Programming (OOP) in Python:

# OOP is a programming paradigm that uses objects and classes to structure code, making it more modular, reusable, and easier to maintain.

# 1. Classes and Objects:
#   - A class is a blueprint for creating objects. It defines the attributes (properties) and methods (behaviors) that objects of the class will have.
#   - An object is an instance of a class, representing a specific entity in the program.
class Car:
    # Class Attribute
    category = "Vehicle"

    # Constructor Method
    def __init__(self, make, model):
        # Instance Attributes
        self.make = make
        self.model = model

    # Method
    def display_info(self):
        print(f"{self.make} {self.model} - {self.category}") # the f here is due to the use of an f-string. F-strings are a way
                                                             # to embed expressions inside string literals using {}
# Creating Objects
car1 = Car("Toyota", "Camry")
car2 = Car("Honda", "Civic")

# Accessing Attributes and Calling Methods
car1.display_info()  # Output: Toyota Camry - Vehicle
car2.display_info()  # Output: Honda Civic - Vehicle

# 2. Attributes and Methods:
#   - Attributes are variables that belong to objects, storing data about the object's state.
#   - Methods are functions defined within a class, representing the behavior or actions that objects can perform.
#   - Attributes and methods can be accessed using dot notation (object.attribute) or (object.method()).
#   - Instance Attributes: Associated with specific instances of a class.
#   - Class Attributes: Shared among all instances of a class.
#   - Instance Methods: Operate on instance attributes and can modify object state.
#   - Class Methods: Operate on class attributes and can be called on the class itself.
#   - Static Methods: Do not operate on instance or class attributes, often used as utility functions.
class Circle:
    pi = 3.14159  # Class Attribute

    def __init__(self, radius):
        self.radius = radius  # Instance Attribute

    def area(self):
        return self.pi * self.radius ** 2  # Instance Method

    @classmethod
    def set_pi(cls, new_pi): # 'cls' is a commonly used convention in class methods to refer to the class itself
        cls.pi = new_pi  # Class Method

    @staticmethod
    def circumference(radius):
        return 2 * Circle.pi * radius  # Static Method

circle1 = Circle(5)
print("Area:", circle1.area())  # Output: 78.53975
Circle.set_pi(3.14)
print("New Area:", circle1.area())  # Output: 78.5
print("Circumference:", Circle.circumference(5))  # Output: 31.4159

# 3. Constructor and Destructor:
#   - Constructor (init method): Special method called when an object is created. Used to initialize instance attributes.
#   - Destructor (del method): Special method called when an object is deleted. Used to perform cleanup tasks.
#   - Python automatically manages memory allocation and deallocation, so destructors are rarely needed.
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print(f"{self.name} is created.")

    def __del__(self):
        print(f"{self.name} is deleted.")

person1 = Person("Alice", 30)  # Output: Alice is created.
del person1  # Output: Alice is deleted.

# 4. Inheritance:
#   - Inheritance allows a class (subclass) to inherit attributes and methods from another class (superclass).
#   - Subclass can extend or override the behavior of the superclass.
#   - Encourages code reuse and promotes modularity.
class Animal:
    def speak(self):
        return "Animal Sound"

class Dog(Animal):
    def speak(self):
        return "Woof!"

class Cat(Animal):
    def speak(self):
        return "Meow!"

dog = Dog()
cat = Cat()
print("Dog:", dog.speak())  # Output: Woof!
print("Cat:", cat.speak())  # Output: Meow!

# 5. Polymorphism:
#   - Polymorphism allows objects of different classes to be treated as objects of a common superclass.
#   - Enables flexibility and extensibility in code design.
#   - Achieved through method overriding, where subclass provides a specific implementation of a method defined in superclass.
class Shape:
    def area(self):
        return 0

class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return 3.14159 * self.radius ** 2

shapes = [Rectangle(5, 4), Circle(3)]
for shape in shapes:
    print("Area:", shape.area())

# 6. Encapsulation:
#   - Encapsulation is the bundling of data and methods that operate on the data within a single unit (class).
#   - Data hiding: Implementation details are hidden from the outside world, and access is restricted to class methods.
#   - Encapsulation promotes data integrity and prevents unauthorized access or modification of data.
class BankAccount:
    def __init__(self, balance):
        self._balance = balance  # Protected Attribute

    def deposit(self, amount):
        """Deposit money into the account."""
        if amount > 0:
            self._balance += amount
            print(f"Deposited ${amount}")
        else:
            print("Invalid deposit amount")

    def withdraw(self, amount):
        """Withdraw money from the account."""
        if amount > 0 and self._balance >= amount:
            self._balance -= amount
            print(f"Withdrew ${amount}")
        else:
            print("Insufficient funds or invalid withdrawal amount")

    def get_balance(self):
        """Get the current balance."""
        return self._balance
    
# Creating an instance of BankAccount
account = BankAccount(1000)

# Accessing balance directly (not encapsulated)
print("Direct Access - Current Balance:", account._balance)  # Output: 1000 (Wrong)

# Making a deposit directly (not encapsulated)
account._balance += 500  # Direct modification (Wrong)
print("Direct Access - Updated Balance:", account._balance)  # Output: 1500 (Wrong)

# Making a withdrawal directly (not encapsulated)
account._balance -= 200  # Direct modification (Wrong)
print("Direct Access - Updated Balance:", account._balance)  # Output: 1300 (Wrong)

# Proper use of encapsulation
print("\nProper Encapsulation:")
print("Initial Balance:", account.get_balance())  # Output: 1000 (Right)
account.deposit(500)  # Using method to deposit (Right)
print("Updated Balance:", account.get_balance())  # Output: 1500 (Right)
account.withdraw(200)  # Using method to withdraw (Right)
print("Updated Balance:", account.get_balance())  # Output: 1300 (Right)

# 7. Abstract Classes and Interfaces:
#   - Abstract Class: A class that cannot be instantiated and serves as a blueprint for other classes.
#   - Interface: A collection of abstract methods that defines a contract for classes that implement it.
#   - Python does not have built-in support for abstract classes and interfaces, but they can be achieved using the 'abc' module.
#   - Abstract methods are defined using the '@abstractmethod' decorator.
from abc import ABC, abstractmethod

class Shape(ABC):
    @abstractmethod
    def area(self):
        pass

class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return 3.14159 * self.radius ** 2

# -------------------------------- Separating classes in different files --------------------------------
# separating a Python project into different files is a common practice, especially when working with object-oriented
# programming (OOP) where each class can be placed in its own file for better organization and modularity

# Separate Python Project into Different Files:

# 1. Create Separate Python Files for Each Class:
#    - Create a separate Python file for each class you want to define.
#    - Define the class in the corresponding file using the 'class' keyword.

# File: person.py
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

# 2. Import Classes into Main Code File:
#    - In your main code file (e.g., 'main.py'), import the classes you need from their respective files using the 'import' statement.
#    - You can then instantiate objects of these classes and use them in your main code.

## File: main.py
#from person import Person

#def main():
    ## Instantiate objects of the Person class
#    person1 = Person("Alice", 30)
#    person2 = Person("Bob", 25)

    ## Use objects in your main code
#    print(person1.name)
#    print(person2.age)

#if __name__ == "__main__":
#    main()

# 3. Organize Files in a Directory Structure:
#    - It's a good practice to organize your files into a directory structure, especially for larger projects.
#    - Create a directory for your project and place all Python files inside it.
#    - You can further organize your files into subdirectories based on functionality or module.

# Example Directory Structure:
# my_project/
# ├── main.py
# ├── person.py
# └── other_module/
#     └── other_file.py

# 4. Avoid Circular Imports:
#    - Be careful to avoid circular imports, where two modules depend on each other. This can lead to runtime errors.
#    - If you encounter circular imports, refactor your code to remove the dependency loop or use techniques like importing modules within functions to delay the import.
