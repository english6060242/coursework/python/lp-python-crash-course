# File Management in Python:

# File management involves operations related to creating, reading, writing, and manipulating files in a computer's file system.

# 1. File Creation and Opening:
#   - Python provides built-in functions for creating and opening files using the 'open()' function.
#   - 'open()' function takes two arguments: file name/path and mode.
#   - Modes: 'r' (read), 'w' (write), 'a' (append), 'b' (binary), '+' (read/write), etc.
#   - By default, 'open()' function opens files in text mode ('t').
file_name = 'example.txt'

# Example: Creating and opening a file in write mode
with open(file_name, 'w') as file:
    file.write("This is a sample text file.\n")
    file.write("It contains some lines of text.\n")

# 2. File Reading:
#   - Python provides methods for reading files, including 'read()', 'readline()', and 'readlines()'.
#   - 'read()' reads the entire file content as a single string.
#   - 'readline()' reads a single line from the file.
#   - 'readlines()' reads all lines from the file and returns them as a list of strings.
with open(file_name, 'r') as file:
    content = file.read()
    print("File Content:")
    print(content)

# 3. File Writing and Appending:
#   - Python allows writing and appending data to files using the 'write()' method.
#   - 'write()' method writes a string to the file.
#   - 'append()' mode ('a') can be used to append data to the end of the file without overwriting existing content.
with open(file_name, 'a') as file:
    file.write("Additional line appended to the file.\n")

# 4. File Manipulation:
#   - Python provides various functions and methods for file manipulation, including renaming, deleting, and checking file existence.
import os

# Renaming a file
new_file_name = 'renamed_example.txt'
os.rename(file_name, new_file_name)
print("File renamed to", new_file_name)

# Deleting a file
os.remove(new_file_name)
print("File", new_file_name, "deleted")

# Checking file existence
if os.path.exists(new_file_name):
    print("File", new_file_name, "exists")
else:
    print("File", new_file_name, "does not exist")

# File management in Python is essential for handling data stored in files and performing various operations such as reading, writing, and manipulation.
