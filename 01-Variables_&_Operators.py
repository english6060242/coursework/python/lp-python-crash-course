# Variables and data types
name = "Alice"      # Strings - Python does not differentiate between "" and ''
age = 30            # Integers/Ints
height = 5.8        # Floating point Value / Float
is_student = True   # Boolean

# Printing variables
print("Name:", name)
print("Age:", age)
print("Height:", height)
print("Is student?", is_student)

# Variables do not have a fixed type; you can assign any value of any type to any variable, 
# but operations need to be performed among consistent types.

print(age + height)        # This is okay (operations between an int and a float are valid since they're both numbers)
print("Hello " + name)     # This is also okay
#print(age + "Hello")      # This is not
print(50 / 5)              # Division always results in a float
print("Hello " * 5)        # Some operations among different types do exist (This will print "Hello " five times)

#-----------------------------------------------Variable Naming Rules in Python---------------------------------------------------

# Variable names can only contain letters (a-z, A-Z), digits (0-9), and underscores (_).
# They must start with a letter or an underscore, but not a digit.
# Variable names are case-sensitive.
# Variable names cannot use names of inbuilt python functions such as print, while, etc

#Examples:
student = "Alice"
_height = 5.8  # Valid: starts with an underscore
aux2 = "hello" #Valid, starts with a letter
# 2cars = 3     # Invalid: starts with a digit

Student = "Bob"
print(student)    # Output: Alice
print(Student)    # Output: Bob

#-------------------------------------------------------- Recommended ------------------------------------------------------------

# Use meaningful names even for loop counters to enhance code readability (avoid using single-letter variable names).
# Be consistent with naming conventions throughout your codebase.
# Follow the Python style guide (PEP 8) for naming conventions: https://www.python.org/dev/peps/pep-0008/
#---------------------------------------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------------------------------------
#                                                     Operators in Python
#---------------------------------------------------------------------------------------------------------------------------------

# Operators are special symbols or keywords that perform operations on operands.
# They allow you to manipulate values and variables in Python expressions and statements.

# 1. Arithmetic Operators:
#   - Addition (+): Adds two operands.
#   - Subtraction (-): Subtracts the right operand from the left operand.
#   - Multiplication (*): Multiplies two operands.
#   - Division (/): Divides the left operand by the right operand (results in float).
#   - Floor Division (//): Divides and returns the integer part of the quotient.
#   - Modulus (%): Returns the remainder of the division.
#   - Exponentiation (**): Raises the left operand to the power of the right operand.

# 2. Comparison Operators:
#   - Equal to (==): Compares if two operands are equal.
#   - Not equal to (!=): Compares if two operands are not equal.
#   - Greater than (>): Checks if the left operand is greater than the right operand.
#   - Less than (<): Checks if the left operand is less than the right operand.
#   - Greater than or equal to (>=): Checks if the left operand is greater than or equal to the right operand.
#   - Less than or equal to (<=): Checks if the left operand is less than or equal to the right operand.

# 3. Logical Operators:
#   - Logical AND (and): Returns True if both operands are true.
#   - Logical OR (or): Returns True if either of the operands is true.
#   - Logical NOT (not): Returns True if the operand is false, and vice versa.

# 4. Assignment Operators:
#   - Assignment (=): Assigns a value to a variable.
#   - Compound Assignment (+=, -=, *=, /=, //=, %=, **=): Performs arithmetic operation and assigns the result to the variable.

# 5. Identity Operators:
#   - Identity (is): Returns True if both operands refer to the same object.
#   - Non-Identity (is not): Returns True if both operands do not refer to the same object.

# 6. Membership Operators:
#   - Membership (in): Returns True if a value is present in a sequence (list, tuple, dictionary, string).
#   - Non-Membership (not in): Returns True if a value is not present in a sequence.

# 7. Bitwise Operators:
#   - Bitwise AND (&): Performs bitwise AND operation.
#   - Bitwise OR (|): Performs bitwise OR operation.
#   - Bitwise XOR (^): Performs bitwise XOR (exclusive OR) operation.
#   - Bitwise NOT (~): Performs bitwise NOT (complement) operation.
#   - Left Shift (<<): Shifts the bits of the left operand to the left by the number of bits specified by the right operand.
#   - Right Shift (>>): Shifts the bits of the left operand to the right by the number of bits specified by the right operand.